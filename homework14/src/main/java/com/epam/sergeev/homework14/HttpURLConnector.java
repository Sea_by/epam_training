package com.epam.sergeev.homework14;

import static com.epam.sergeev.homework14.Constants.COUNTRY_NAME;
import static com.epam.sergeev.homework14.Constants.POST;
import static com.epam.sergeev.homework14.Constants.URL_ADDRESS;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

public class HttpURLConnector implements Connector {

    @Override
    public List<Pair> doGet(String countryName) {
        try {
            URL url = new URL(URL_ADDRESS + "?" + COUNTRY_NAME + "=" + countryName);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                Parser parser = new Parser();
                return parser.parseResponse(connection.getInputStream());
            } else {
                Logger.getLogger(Homework14.class
                        .getName()).log(Level.SEVERE, "Bad response");
            }
        } catch (IOException ex) {
            Logger.getLogger(HttpURLConnector.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Pair> doPost(String countryName) {
        try {
            String postData = COUNTRY_NAME + "=" + countryName;

            HttpURLConnection connection = (HttpURLConnection) new URL(URL_ADDRESS).openConnection();
            connection.setRequestMethod(POST);
            connection.setDoOutput(true);

            try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                wr.writeBytes(postData);
                wr.flush();
            }

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                Parser parser = new Parser();
                return parser.parseResponse(connection.getInputStream());
            } else {
                Logger.getLogger(Homework14.class
                        .getName()).log(Level.SEVERE, "Bad response");
            }
        } catch (IOException ex) {
            Logger.getLogger(HttpURLConnector.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }
}
