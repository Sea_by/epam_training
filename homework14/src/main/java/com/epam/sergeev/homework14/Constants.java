package com.epam.sergeev.homework14;

public abstract class Constants {

    public static final String URL_ADDRESS = "http://www.webservicex.net/country.asmx/GetGMTbyCountry";
    public static final String POST = "POST";
    public static final String COUNTRY_NAME = "CountryName";

}
