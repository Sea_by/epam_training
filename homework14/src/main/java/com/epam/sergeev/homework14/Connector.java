package com.epam.sergeev.homework14;

import java.util.List;
import javafx.util.Pair;

public interface Connector {

    public List<Pair> doGet(String countryName);

    public List<Pair> doPost(String countryName);

}
