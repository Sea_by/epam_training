package com.epam.sergeev.homework14;

import static com.epam.sergeev.homework14.Constants.COUNTRY_NAME;
import static com.epam.sergeev.homework14.Constants.URL_ADDRESS;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpStatus;

public class HttpClientConnector implements Connector {

    @Override
    public List<Pair> doGet(String countryName) {
        GetMethod get = new GetMethod(URL_ADDRESS + "?" + COUNTRY_NAME + "=" + countryName);
        get.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(3, false));

        return doRequest(get);
    }

    @Override
    public List<Pair> doPost(String countryName) {
        PostMethod post = new PostMethod(URL_ADDRESS);
        post.addParameter("CountryName", countryName);
        return doRequest(post);
    }

    private List<Pair> doRequest(HttpMethodBase method) {
        HttpClient client = new HttpClient();
        try {
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            Parser parser = new Parser();
            return parser.parseResponse(method.getResponseBodyAsStream());
        } catch (IOException ex) {
            Logger.getLogger(Homework14.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            method.releaseConnection();
        }
        return new ArrayList<>();
    }
}
