package com.epam.sergeev.homework14;

import java.util.List;
import javafx.util.Pair;

public class Homework14 {

    public static void main(String args[]) {
        if (args.length > 0) {
            Connector connector = new HttpURLConnector();
            System.out.println("doHttpGet");
            print(connector.doGet(args[0]));
            System.out.println("doHttpPost");
            print(connector.doPost(args[0]));

            connector = new HttpClientConnector();
            System.out.println("HttpClient getMethod");
            print(connector.doGet(args[0]));
            System.out.println("HttpClient postMethod");
            print(connector.doPost(args[0]));
        } else {
            System.err.println("No input!");
        }
    }

    public static void print(List<Pair> list) {
        list.forEach((item) -> {
            System.out.println(item.getKey() + " " + item.getValue());
        });
    }
}
