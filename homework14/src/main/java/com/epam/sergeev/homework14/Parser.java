package com.epam.sergeev.homework14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

public class Parser {

    private static final String GMT = "GMT";
    private static final String NAME = "Name";

    public List<Pair> parseResponse(InputStream inputStream) throws IOException {
        List<Pair> answer = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(inputStream))) {

            XMLStreamReader xmlr = XMLInputFactory.newInstance()
                    .createXMLStreamReader(in);

            while (xmlr.hasNext()) {
                xmlr.next();
                if (xmlr.hasText() && xmlr.getText().trim().length() > 0) {
                    if (xmlr.getText().equals(GMT)) {
                        xmlr.next();
                        xmlr.next();
                        Pair pair = new Pair(GMT, xmlr.getText());
                        answer.add(pair);
                    }
                    if (xmlr.getText().equals(NAME)) {
                        xmlr.next();
                        xmlr.next();
                        Pair pair = new Pair(NAME, xmlr.getText());
                        answer.add(pair);
                    }
                }
            }
        } catch (javax.xml.stream.XMLStreamException ex) {
            Logger.getLogger(Homework14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return answer;
    }
}
