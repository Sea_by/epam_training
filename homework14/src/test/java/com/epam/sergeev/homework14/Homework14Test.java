package com.epam.sergeev.homework14;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import junit.framework.TestCase;
import org.junit.Test;

public class Homework14Test extends TestCase {

    private final String countryName = "Belarus";

    @Test
    public void testResponseResult() {
        List<Pair> result = initPairList();

        Connector connector = new HttpURLConnector();
        assertEquals(connector.doGet(countryName), result);
        assertEquals(connector.doPost(countryName), result);

        connector = new HttpClientConnector();
        assertEquals(connector.doGet(countryName), result);
        assertEquals(connector.doPost(countryName), result);
    }

    @Test
    public void testParser_parseResponse() throws UnsupportedEncodingException, IOException {
        String input = "<string xmlns=\"http://www.webserviceX.NET\">"
                + "&lt;NewDataSet&gt;"
                + "&lt;Table&gt;"
                + "&lt;GMT&gt;+2&lt;/GMT&gt;"
                + "&lt;Name&gt;Belarus&lt;/Name&gt;"
                + "&lt;/Table&gt;"
                + "&lt;Table&gt;"
                + "&lt;GMT&gt;+2&lt;/GMT&gt;"
                + "&lt;Name&gt;Belarus&lt;/Name&gt;"
                + "&lt;/Table&gt;"
                + "&lt;/NewDataSet&gt;"
                + "</string>";

        List<Pair> result = initPairList();
        Parser parser = new Parser();
        InputStream is = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8.name()));

        assertEquals(parser.parseResponse(is), result);
    }

    private List<Pair> initPairList() {
        List<Pair> result = new ArrayList<>();
        Pair timePair = new Pair("GMT", "+2");
        Pair namePair = new Pair("Name", countryName);
        result.add(timePair);
        result.add(namePair);
        result.add(timePair);
        result.add(namePair);
        return result;
    }
}
