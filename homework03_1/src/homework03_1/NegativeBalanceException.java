package homework03_1;

class NegativeBalanceException extends RuntimeException {

    public NegativeBalanceException() {
        super("No such money");
    }

}
