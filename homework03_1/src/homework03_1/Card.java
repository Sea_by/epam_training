package homework03_1;

public class Card {

    private final String name;
    protected double balance = 0.0D;

    public Card(String name) {
        this.name = name;
    }

    public Card(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public synchronized void addMoney(double value) {
        this.balance += value;
    }

    public synchronized void removeMoney(double value) {
        this.balance -= value;
    }

    public double getOtherBalance(double ratio) {
        return this.balance * ratio;
    }
}
