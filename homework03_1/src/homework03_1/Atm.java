package homework03_1;

public class Atm {

    protected Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public void removeMoney(double value) {
        card.removeMoney(value);
    }

    public void addMoney(double value) {
        card.addMoney(value);
    }
}
