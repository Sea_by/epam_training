package homework03_1;

public class CreditCard extends Card {

    public CreditCard(String name) {
        super(name);
    }

    public CreditCard(String name, double balance) {
        super(name, balance);
    }

}
