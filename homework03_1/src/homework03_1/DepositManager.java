package homework03_1;

import java.util.Random;

public class DepositManager extends Atm implements Runnable {

    public DepositManager(Card card) {
        super(card);
    }

    @Override
    public void run() {
        Random rnd = new Random();
        System.out.println(Thread.currentThread().getName() + " : start deposit; balance : " + card.balance);
        card.addMoney(rnd.nextInt(5) + 5);
        System.out.println(Thread.currentThread().getName() + " : end deposit; balance : " + card.balance);
    }

}
