package homework03_1;

import java.util.Random;

public class WithdrawManager extends Atm implements Runnable {

    public WithdrawManager(Card card) {
        super(card);
    }

    @Override
    public void run() {
        Random rnd = new Random();
        System.out.println(Thread.currentThread().getName() + " : start withdraw; balance : " + card.balance);
        card.removeMoney(rnd.nextInt(5) + 5);
        System.out.println(Thread.currentThread().getName() + " : end withdraw; balance : " + card.balance);
    }

}
