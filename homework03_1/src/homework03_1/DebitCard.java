package homework03_1;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DebitCard extends Card {

    public DebitCard(String name) {
        super(name);
    }

    public DebitCard(String name, double balance) {
        super(name, balance);
    }

    @Override
    public synchronized void removeMoney(double value) {
        try {
            if (this.getBalance() - value >= 0) {
                balance -= value;
            } else {
                throw new NegativeBalanceException();
            }
        } catch (NegativeBalanceException ex) {
            Logger.getLogger(DebitCard.class.getName()).log(Level.WARNING, "No such money!");
        }
    }
}
