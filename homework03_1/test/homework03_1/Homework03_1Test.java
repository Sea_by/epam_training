package homework03_1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Assert;
import org.junit.Test;

public class Homework03_1Test {

    @Test
    public void testCreateCard() {
        Card card = new Card("name", 100);
        Assert.assertEquals(100, card.getBalance(), 0.00001);
    }

    @Test
    public void testCreateCardWithoutBalance() {
        Card card = new Card("name");
        Assert.assertEquals(0, card.getBalance(), 0.00001);
    }

    @Test
    public void testAddMoney() {
        Card card = new Card("name", 5.55);
        Atm atm = new Atm(card);
        atm.addMoney(0.45);
        Assert.assertEquals(6, card.getBalance(), 0.00001);
    }

    @Test
    public void testRemoveMoney() {
        Card card = new Card("name", 97.22);
        Atm atm = new Atm(card);
        atm.removeMoney(37.89);
        Assert.assertEquals(59.33, card.getBalance(), 0.00001);
    }

    @Test
    public void testOtherBalance() {
        Card card = new Card("name", 100);
        Assert.assertEquals(34.56, card.getOtherBalance(0.3456), 0.00001);
    }

    @Test
    public void testCreditCard_NegativeBalance() {
        Card card = new CreditCard("name");
        Atm atm = new Atm(card);
        atm.removeMoney(10);
        Assert.assertEquals(-10, card.getBalance(), 0.00001);
    }

    @Test //(expected = NegativeBalanceException.class)
    public void testDebitCard_NegativeBalance() {
        Card card = new DebitCard("name");
        Atm atm = new Atm(card);
        atm.removeMoney(10);
    }

    @Test
    public void testMultiThreading() {
        Card card = new CreditCard("name", 500);
        ExecutorService executors = Executors.newFixedThreadPool(10);
        while ((card.balance > 450) && (card.balance < 550)) {
            executors.submit(new DepositManager(card));
            executors.submit(new WithdrawManager(card));
            executors.submit(new DepositManager(card));
            executors.submit(new WithdrawManager(card));
            executors.submit(new DepositManager(card));
            executors.submit(new WithdrawManager(card));
        }
        executors.shutdown();
    }
}
