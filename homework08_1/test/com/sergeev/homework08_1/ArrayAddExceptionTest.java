package com.sergeev.homework08_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public class ArrayAddExceptionTest {

    @Test
    public void testAddElement() {
        LimitedList limitedList = new LimitedList(2);
        limitedList.add(1);
        limitedList.add(2);
    }

    @Test(expected = ArrayAddException.class)
    public void testAddElementFails() {
        LimitedList limitedList = new LimitedList();
        limitedList.add(1);
    }

    @Test
    public void testAddCollection() {
        int[] resultArray = new int[]{0, 1, 2, 2, 3, 5, 7, 8, 9};
        LimitedList limitedList = new LimitedList(9);
        limitedList.addAll(Arrays.asList(resultArray));
    }

    @Test(expected = ArrayAddException.class)
    public void testAddCollectionFails() {
        List list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);

        LimitedList limitedList = new LimitedList(2);

        limitedList.addAll(list);
    }

}
