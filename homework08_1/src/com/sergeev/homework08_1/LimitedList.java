package com.sergeev.homework08_1;

import java.util.ArrayList;
import java.util.Collection;

public class LimitedList extends ArrayList<Object> {

    private final int capacity;

    public LimitedList() {
        this.capacity = 0;
    }

    public LimitedList(int capacity, Collection<? extends Object> c) {
        super(c);
        this.capacity = capacity;
    }

    public LimitedList(int capacity, int initialCapacity) {
        super(initialCapacity);
        this.capacity = capacity;
    }

    public LimitedList(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean add(Object e) {
        if (this.size() + 1 <= capacity) {
            return super.add(e);
        } else {
            throw new ArrayAddException("full array. can't add");
        }
    }

    @Override
    public void add(int index, Object element) {
        if (this.size() + 1 <= capacity) {
            super.add(index, element);
        } else {
            throw new ArrayAddException("full array. can't add");
        }
    }

    @Override
    public boolean addAll(Collection<? extends Object> c) {
        if (this.size() + c.size() <= capacity) {
            return super.addAll(c);
        } else {
            throw new ArrayAddException("not enough space for add collection");
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends Object> c) {
        if (this.size() + c.size() <= capacity) {
            return super.addAll(index, c);
        } else {
            throw new ArrayAddException("not enough space for add collection");
        }
    }

}
