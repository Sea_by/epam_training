package com.sergeev.homework08_1;

public class ArrayAddException extends RuntimeException {

    public ArrayAddException(String message) {
        super(message);
    }

}
