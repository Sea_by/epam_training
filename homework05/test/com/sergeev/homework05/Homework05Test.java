package com.sergeev.homework05;

import org.junit.Assert;
import org.junit.Test;

public class Homework05Test {

    @Test(expected = ArchitecturalFolderException.class)
    public void testCutter_FileAttachment() {
        Homework05.cutter("some/file.txt/some.txt");
    }

    @Test
    public void testCutter() {
        String[] result = new String[]{"some", "file", "some.txt"};
        String[] cutter = Homework05.cutter("some/file/some.txt");
        Assert.assertArrayEquals(result, cutter);
    }

}
