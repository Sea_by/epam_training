package com.sergeev.homework05;

public class ArchitecturalFolderException extends RuntimeException {

    public ArchitecturalFolderException() {
        super("the attachment in the file found!");
    }

}
