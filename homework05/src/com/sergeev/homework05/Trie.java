package com.sergeev.homework05;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class Trie implements Serializable {

    private final Node root;

    public Trie() {
        this.root = new Node();
    }

    public Node getRoot() {
        return root;
    }

    public void addAll(String[] path) {
        Node crawl = root;
        for (String item : path) {
            if (crawl.children.containsKey(item)) {
                crawl = crawl.children.get(item);
            } else {
                final Node temp = new Node();
                crawl.children.put(item, temp);
                crawl = temp;
            }
        }
    }

    public void printTrie(Node crawl, int count) {
        if (!crawl.children.isEmpty()) {
            Set<Map.Entry<String, Node>> set = crawl.children.entrySet();
            for (Map.Entry<String, Node> entry : set) {
                if (entry.getValue() != null) {
                    String indent = "";
                    for (int i = 0; i < count; i++) {
                        indent += "\t";
                    }
                    System.out.println(indent + entry.getKey());
                    printTrie(entry.getValue(), count + 1);
                }
            }
        }
    }
}
