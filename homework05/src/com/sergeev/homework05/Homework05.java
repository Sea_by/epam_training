package com.sergeev.homework05;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static java.lang.System.exit;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Homework05 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Trie trie = new Trie();

        while (true) {
            String input = in.nextLine();
            switch (input) {
                case "print":
                    trie.printTrie(trie.getRoot(), 0);
                    break;
                case "exit":
                    exit(0);
                    break;
                case "save":
                    try (FileOutputStream fos = new FileOutputStream("testSer.ser");
                            ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                        oos.writeObject(trie);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Homework05.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Homework05.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "load":
                    try (FileInputStream fis = new FileInputStream("testSer.ser");
                            ObjectInputStream ois = new ObjectInputStream(fis)) {
                        trie = (Trie) ois.readObject();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Homework05.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Homework05.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                default:
                    try {
                        String[] result = cutter(input);
                        trie.addAll(result);
                    } catch (ArchitecturalFolderException afe) {
                        Logger.getLogger(Homework05.class.getName()).log(Level.WARNING, "the attachment in the file found!");
                    }
                    break;
            }
        }
    }

    public static String[] cutter(String input) {
        String[] result = input.split("/");
        int counter = 0;
        for (String item : result) {
            if ((item.contains(".")) && (counter != result.length - 1)) {
                throw new ArchitecturalFolderException();
            }
            counter++;
        }
        return result;
    }
}
