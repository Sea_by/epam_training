package com.sergeev.homework05;

import java.io.Serializable;
import java.util.HashMap;

public class Node implements Serializable {

    HashMap<String, Node> children;

    public Node() {
        children = new HashMap<>();
    }
}
