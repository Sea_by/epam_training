package com.epam.sergeev.homework15.web;

import com.epam.sergeev.homework15.model.User;
import com.epam.sergeev.homework15.service.UserService;
import com.epam.sergeev.homework15.service.UserServiceImpl;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    UserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User(req.getParameter("user"));
        req.setAttribute("user", user);
        userService.saveUser(user);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/products");
        rd.forward(req, resp);
    }

}
