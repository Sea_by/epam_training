package com.epam.sergeev.homework15.service;

import com.epam.sergeev.homework15.model.User;
import com.epam.sergeev.homework15.repo.UserRepo;

public class UserServiceImpl implements UserService {

    private final UserRepo userRepo = UserRepo.getInstance();

    @Override
    public void saveUser(User user) {
        userRepo.addUser(user);
    }

    @Override
    public User getUser(String name) {
        return userRepo.getUser(name);
    }

    @Override
    public User getCurrentUser() {
        return userRepo.getCurrentUser();
    }

}
