package com.epam.sergeev.homework15.repo;

import com.epam.sergeev.homework15.model.User;
import java.util.ArrayList;
import java.util.List;

public class UserRepo {

    private User currentUser;
    private final List<User> users;
    private final User defUser = new User("defUser");

    private static UserRepo instance;

    public static synchronized UserRepo getInstance() {
        if (instance == null) {
            instance = new UserRepo();
        }
        return instance;
    }

    public UserRepo() {
        this.users = new ArrayList<>();
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void addUser(User user) {
        users.add(user);
        currentUser = user;
    }

    public List<User> getUsers() {
        return users;
    }

    public User getUser(String name) {
        for (User user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return defUser;
    }
}
