package com.epam.sergeev.homework15.web;

import com.epam.sergeev.homework15.service.Calculator;
import com.epam.sergeev.homework15.model.Product;
import com.epam.sergeev.homework15.service.ProductService;
import com.epam.sergeev.homework15.service.ProductServiceImpl;
import com.epam.sergeev.homework15.service.UserService;
import com.epam.sergeev.homework15.service.UserServiceImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/ticket")
public class TicketServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService ps = new ProductServiceImpl();
        UserService us = new UserServiceImpl();
        Calculator calculator = new Calculator();

        List<Product> selectedProducts = new ArrayList<>();

        String[] selected = req.getParameterValues("products");

        if (selected != null) {
            for (String item : selected) {
                selectedProducts.add(ps.getProduct(item));
            }
        }

        System.out.println(us.getCurrentUser());

        req.setAttribute("user", us.getCurrentUser());
        req.setAttribute("selected", selectedProducts);
        req.setAttribute("sum", calculator.calculate(selectedProducts));

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/ticket.jsp");
        rd.forward(req, resp);
    }

}
