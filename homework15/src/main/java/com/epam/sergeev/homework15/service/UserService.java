package com.epam.sergeev.homework15.service;

import com.epam.sergeev.homework15.model.User;

public interface UserService {

    public void saveUser(User user);

    public User getUser(String name);

    public User getCurrentUser();
}
