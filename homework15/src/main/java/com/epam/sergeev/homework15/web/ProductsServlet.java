package com.epam.sergeev.homework15.web;

import com.epam.sergeev.homework15.service.ProductService;
import com.epam.sergeev.homework15.service.ProductServiceImpl;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/products")
public class ProductsServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService ps = new ProductServiceImpl();
        req.setAttribute("products", ps.getAll());

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/products.jsp");
        rd.forward(req, resp);
    }

}
