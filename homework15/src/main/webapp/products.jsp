<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Products</title>
    </head>
    <body>
        <h1>Hello ${user.name}!</h1>
        <h2>Make you order</h2>
        <form method="post" action="ticket" >
            <p> Products
                <br>
                <select name = "products" multiple>
                    <c:forEach items="${products}" var="item">
                        <option value="${item.name}">${item.name}---${item.price} $</option>
                    </c:forEach>
                </select>
            </p>
            <Input name="Submit_B" type="submit" value="Submit" />  
        </form>
    </body>
</html>
