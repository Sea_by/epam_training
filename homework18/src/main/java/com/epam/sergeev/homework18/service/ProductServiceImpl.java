package com.epam.sergeev.homework18.service;

import com.epam.sergeev.homework18.h2.H2ProductRepository;
import com.epam.sergeev.homework18.model.Product;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final H2ProductRepository h2ProductRepository = H2ProductRepository.getInstance();

    @Override
    public void saveProduct(Product product) {
        h2ProductRepository.addProduct(product);
    }

    @Override
    public Product getProduct(String name) {
        return h2ProductRepository.getProduct(name);
    }

    @Override
    public List<Product> getAll() {
        return h2ProductRepository.getAll();
    }

    @Override
    public void saveOrder(List<Product> products) {

    }

}
