package com.epam.sergeev.homework18.service;

import com.epam.sergeev.homework18.h2.H2ProductTicketRepository;
import com.epam.sergeev.homework18.model.Product;
import com.epam.sergeev.homework18.model.ProductTicket;
import com.epam.sergeev.homework18.model.Ticket;
import java.util.List;

public class ProductTicketService {

    H2ProductTicketRepository h2ProductTicketRepository = H2ProductTicketRepository.getInstance();

    public void save(ProductTicket productTicket) {
        h2ProductTicketRepository.saveProductTicket(productTicket);
    }

    public List<Product> getByTicket(Ticket ticket) {
        return h2ProductTicketRepository.getByTicket(ticket);
    }
}
