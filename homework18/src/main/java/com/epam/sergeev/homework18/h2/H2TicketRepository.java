package com.epam.sergeev.homework18.h2;

import com.epam.sergeev.homework18.filters.RequestLoggingFilter;
import com.epam.sergeev.homework18.model.Ticket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H2TicketRepository {

    private static H2TicketRepository instance;

    private final DatabaseConfig connect = new DatabaseConfig();
    private static Statement statement;

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2TicketRepository getInstance() {
        if (instance == null) {
            instance = new H2TicketRepository();
        }
        return instance;
    }

    private H2TicketRepository() {
    }

    public void addTicket(Ticket ticket) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            statement = con.createStatement();
            statement.execute("INSERT INTO PUBLIC.TICKET(TOTAL,ID_USER) VALUES ("
                    + ticket.getTotal() + "," + ticket.getUser().getId() + ")");
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "error query addTicket", ex);
        }
    }

    public int getTicketId(Ticket ticket) {
        int id = 0;
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery("select * from TICKET WHERE ID_USER='"
                    + ticket.getUser().getId() + "'");
            rs.next();

            id = rs.getInt("ID_TICKET");
        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error query getUserByName ", sqlEx);
        }
        return id;
    }
}
