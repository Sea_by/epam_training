package com.epam.sergeev.homework18.h2;

import com.epam.sergeev.homework18.filters.RequestLoggingFilter;
import com.epam.sergeev.homework18.model.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H2UserRepository {

    public static H2UserRepository instance;

    private final DatabaseConfig connect = new DatabaseConfig();
    private static Statement statement;

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2UserRepository getInstance() {
        if (instance == null) {
            instance = new H2UserRepository();
        }
        return instance;
    }

    private H2UserRepository() {
    }

    public void saveUser(String name) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            statement = con.createStatement();
            statement.execute("INSERT INTO PUBLIC.USER(NAME) VALUES ('" + name + "')");
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "error connection or query ", ex);
        }
    }

    public User getUserByName(String name) {
        User defUser = new User("defUser");
        defUser.setId(0);
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery("select * from USER WHERE NAME='" + name + "'");
            rs.next();

            defUser = new User(rs.getString("NAME"));
            defUser.setId(rs.getInt("id_user"));
        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error query getUserByName ", sqlEx);
        }
        return defUser;
    }

}
