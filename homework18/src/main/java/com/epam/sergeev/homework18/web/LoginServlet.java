package com.epam.sergeev.homework18.web;

import com.epam.sergeev.homework18.service.UserService;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private final String ON = "on";
    private final String USER = "user";

    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(getServletContext().getContextPath() + "/login.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ON.equals(req.getParameter("checkbox"))) {
            HttpSession session = req.getSession();
            session.setMaxInactiveInterval(3 * 60);
            Cookie userName = new Cookie(USER, req.getParameter(USER));
            userName.setMaxAge(60);

            session.setAttribute(USER, userName);

            userService = new UserService();
            userService.saveUser(req.getParameter(USER));

            req.setAttribute("order", new ArrayList<>());

            resp.addCookie(userName);
            resp.sendRedirect(getServletContext().getContextPath() + "/products");
        } else {
            resp.sendRedirect(getServletContext().getContextPath() + "/403.html");
        }
    }
}
