package com.epam.sergeev.homework18.service;

import com.epam.sergeev.homework18.h2.H2UserRepository;
import com.epam.sergeev.homework18.model.User;

public class UserService {

    private final H2UserRepository h2UserRepository = H2UserRepository.getInstance();

    public void saveUser(String name) {
        h2UserRepository.saveUser(name);
    }

    public User getUserByName(String userName) {
        return h2UserRepository.getUserByName(userName);
    }
}
