package com.epam.sergeev.homework18.filters;

import com.epam.sergeev.homework18.h2.DatabaseConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DBDeployListener implements ServletContextListener {

    private final DatabaseConfig connect = new DatabaseConfig();

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    private static final String CREATETABLES = "create_tables.sql";
    private static final String INITTABLES = "insert_def_values.sql";

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        try {
            Class.forName("org.h2.Driver");
            if (executeScript(CREATETABLES)) {
                if (executeScript(INITTABLES)) {
                    LOG.info("db creation scripts was successfully executed");
                } else {
                    LOG.info("tables created whithout any data");
                }
            } else {
                LOG.info("db already exist");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, "Can't load db driver", ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * execute sql-script from resurce file
     *
     * @param filename resurce filename with sql-script
     * @return true if script was successfully executed, false otherwise
     */
    private boolean executeScript(String filename) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword());
                Statement stmt = con.createStatement()) {

            stmt.executeUpdate(readFileAsText(filename));
            return true;
        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error executing script", sqlEx);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "error of reading sql-query ", ex);
        }
        return false;
    }

    private String readFileAsText(String filename) throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String readString;
        String query = "";

        while ((readString = br.readLine()) != null) {
            query += readString;
        }
        return query;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
