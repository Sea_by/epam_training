package com.epam.sergeev.homework18.model;

public class Ticket {

    private int id_ticket;
    private final User user;
    private double total;

    public Ticket(User user, double total) {
        this.user = user;
        this.total = total;
    }

    public int getId() {
        return id_ticket;
    }

    public void setId(int id) {
        this.id_ticket = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public User getUser() {
        return user;
    }

}
