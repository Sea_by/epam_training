package com.epam.sergeev.homework18.h2;

import com.epam.sergeev.homework18.filters.RequestLoggingFilter;
import com.epam.sergeev.homework18.model.Product;
import com.epam.sergeev.homework18.model.ProductTicket;
import com.epam.sergeev.homework18.model.Ticket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H2ProductTicketRepository {

    public static H2ProductTicketRepository instance;

    private final DatabaseConfig connect = new DatabaseConfig();
    private static PreparedStatement pstmt;
    private static Statement statement;
    private H2ProductRepository h2ProductRepository = H2ProductRepository.getInstance();

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2ProductTicketRepository getInstance() {
        if (instance == null) {
            instance = new H2ProductTicketRepository();
        }
        return instance;
    }

    private H2ProductTicketRepository() {
    }

    public void saveProductTicket(ProductTicket productTicket) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            pstmt = con.prepareStatement("Insert into PUBLIC.PRODUCT_TICKET Values(?,?,?)");
            pstmt.setInt(1, productTicket.getProduct().getId());
            pstmt.setInt(2, productTicket.getTicket().getId());
            pstmt.setLong(3, productTicket.getCount());
            pstmt.execute();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "error connection or query ", ex);
        }
    }

    public List<Product> getByTicket(Ticket ticket) {
        List<Product> order = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM PRODUCT_TICKET where id_ticket =" + ticket.getId());

            System.out.println(rs);

            while (rs.next()) {
                order.add(
                        new Product(h2ProductRepository
                                .getProductById(rs.getInt("id_product")).getName(),
                                (double) rs.getInt("COUNT")));
            }

        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error query getByTicket ", sqlEx);
        }
        return order;
    }

}
