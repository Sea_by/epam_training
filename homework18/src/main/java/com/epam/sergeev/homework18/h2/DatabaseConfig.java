package com.epam.sergeev.homework18.h2;

public class DatabaseConfig {

    private static final String URL = "jdbc:h2:~/test";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    public String getUser() {
        return USER;
    }

    public String getPassword() {
        return PASSWORD;
    }

    public String getUrl() {
        return URL;
    }
}
