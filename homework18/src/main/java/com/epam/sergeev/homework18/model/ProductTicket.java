package com.epam.sergeev.homework18.model;

public class ProductTicket {

    private int id;
    private final Product product;
    private final Ticket ticket;
    private final int count;

    public ProductTicket(Product product, Ticket ticket, int count) {
        this.product = product;
        this.ticket = ticket;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public Product getProduct() {
        return product;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
