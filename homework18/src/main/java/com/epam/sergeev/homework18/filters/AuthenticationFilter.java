package com.epam.sergeev.homework18.filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        LOG.info("AuthenticationFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String[] exclusions = new String[]{
            req.getContextPath() + "/",
            req.getContextPath() + "/login",
            req.getContextPath() + "/login.html",
            req.getContextPath() + "/403.html"
        };

        String uri = req.getRequestURI();
        LOG.log(Level.INFO, "Requested Resource::{0}", uri);

        HttpSession session = req.getSession(false);
        boolean flag = true;

        for (String item : exclusions) {
            if (item.equals(uri)) {
                flag = false;
            }
        }

        if ((flag) && (session == null)) {
            LOG.info("Unauthorized access request");
            res.sendRedirect("403.html");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
