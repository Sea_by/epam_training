package com.epam.sergeev.homework18.service;

import com.epam.sergeev.homework18.model.Product;
import java.util.List;

public interface ProductService {

    public void saveProduct(Product product);

    public Product getProduct(String name);

    public List<Product> getAll();

    public void saveOrder(List<Product> products);
}
