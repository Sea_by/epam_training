package com.epam.sergeev.homework18.service;

import com.epam.sergeev.homework18.h2.H2TicketRepository;
import com.epam.sergeev.homework18.model.Ticket;

public class TicketService {

    private final H2TicketRepository h2TicketRepository = H2TicketRepository.getInstance();

    public void addTicket(Ticket ticket) {
        h2TicketRepository.addTicket(ticket);
    }

    public int getTicketId(Ticket ticket) {
        return h2TicketRepository.getTicketId(ticket);
    }
}
