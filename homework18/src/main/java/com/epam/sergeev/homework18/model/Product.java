package com.epam.sergeev.homework18.model;

public class Product {

    private int id_product;
    private final String name;
    private final Double price;

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id_product;
    }

    public void setId(int id) {
        this.id_product = id;
    }

}
