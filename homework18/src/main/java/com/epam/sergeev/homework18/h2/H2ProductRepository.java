package com.epam.sergeev.homework18.h2;

import com.epam.sergeev.homework18.filters.RequestLoggingFilter;
import com.epam.sergeev.homework18.model.Product;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H2ProductRepository {

    private final Product defProduct = new Product("defProduct", 0.0);

    private static H2ProductRepository instance;

    private final DatabaseConfig connect = new DatabaseConfig();
    private static PreparedStatement pstmt;

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2ProductRepository getInstance() {
        if (instance == null) {
            instance = new H2ProductRepository();
        }
        return instance;
    }

    private H2ProductRepository() {
    }

    public void addProduct(Product product) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            pstmt = con.prepareStatement("Insert into PUBLIC.PRODUCT Values(?,?)");
            pstmt.setString(1, product.getName());
            pstmt.setDouble(2, product.getPrice());
            pstmt.executeQuery();

        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "error query addProduct ", ex);
        }
    }

    public Product getProduct(String name) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            pstmt = con.prepareStatement("select * from PUBLIC.PRODUCT WHERE NAME=?");
            pstmt.setString(1, name);
            ResultSet rs = pstmt.executeQuery();
            rs.next();

            return toProduct(rs);
        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error query getProduct ", sqlEx);
        }
        return defProduct;
    }

    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword());
                Statement stmt = con.createStatement()) {

            ResultSet rs;

            rs = stmt.executeQuery("select * from Public.PRODUCT");

            while (rs.next()) {
                Product product = toProduct(rs);
                products.add(product);
            }
        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error query getProducts ", sqlEx);
        }
        return products;
    }

    private Product toProduct(final ResultSet rs) throws SQLException {
        Product product = new Product(rs.getString("NAME"), rs.getDouble("PRICE"));
        product.setId(rs.getInt("ID_PRODUCT"));
        return product;
    }

    public Product getProductById(int Id_product) {
        try (Connection con = DriverManager.getConnection(connect.getUrl(),
                connect.getUser(), connect.getPassword())) {
            pstmt = con.prepareStatement("select * from PUBLIC.PRODUCT WHERE ID_PRODUCT=?");
            pstmt.setInt(1, Id_product);
            ResultSet rs = pstmt.executeQuery();
            rs.next();

            return toProduct(rs);
        } catch (SQLException sqlEx) {
            LOG.log(Level.SEVERE, "error query getProductById ", sqlEx);
        }
        return defProduct;
    }

}
