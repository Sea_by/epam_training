<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ticket</title>
    </head>
    <body>
        <h1>Dear ${user.value}, your order!</h1>
        <c:forEach items="${summary}" var="item">
            <br>${item.name}---${item.price}
        </c:forEach>
        <br> Total: $ ${sum}  
    </body>
</html>
