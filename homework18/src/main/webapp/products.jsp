<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Products</title>
    </head>
    <body>
        <h1>Hello ${user.value}!</h1>

        <form method="post" action="products">
            <h2>You have already chosen:</h2>
            <c:forEach items="${order}" var="item">
                <p> ${item.name}---${item.price} $ </p>
            </c:forEach>

            <h2>Make you order</h2>
            <p> Products
                <br>
                <select name = "products" multiple>
                    <c:forEach items="${products}" var="item">
                        <option value="${item.name}">${item.name}---${item.price} $</option>
                    </c:forEach>
                </select>
            </p>             

            <Input name="Add" type="submit" value="Add Item"/>             
            <Input name="Submit" type="submit" value="Submit"/>  
        </form>
    </body>
</html>
