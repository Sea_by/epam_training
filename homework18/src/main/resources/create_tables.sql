CREATE TABLE Ticket
(	id_ticket             CHAR(18)  NOT NULL AUTO_INCREMENT,
	total                 DOUBLE ,
	id_user               CHAR(18)  NOT NULL 
);

CREATE TABLE Product
(	id_product            CHAR(18)  NOT NULL AUTO_INCREMENT,
	name                  VARCHAR(20) ,
	price                 DOUBLE 
);

CREATE TABLE User
(	id_user               CHAR(18)  NOT NULL AUTO_INCREMENT,
	name                  VARCHAR(20) 
);

CREATE TABLE Product_Ticket
(	id_product            CHAR(18)  NOT NULL ,
	id_ticket             CHAR(18)  NOT NULL ,
	count                 INTEGER 
);

ALTER TABLE Product 
ADD CONSTRAINT XPKProduct  PRIMARY KEY (id_product);

ALTER TABLE Ticket 
ADD CONSTRAINT XPKTicket  PRIMARY KEY (id_ticket);

ALTER TABLE Ticket 
ADD CONSTRAINT R_3  FOREIGN KEY (id_user) REFERENCES User (id_user)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;

ALTER TABLE User 
ADD CONSTRAINT XPKUser  PRIMARY KEY (id_user);

ALTER TABLE Product_Ticket 
ADD CONSTRAINT XPKProduct_Ticket  PRIMARY KEY (id_product,id_ticket);

ALTER TABLE Product_Ticket 
ADD CONSTRAINT R_5  FOREIGN KEY (id_product) REFERENCES Product (id_product)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;

ALTER TABLE Product_Ticket 
ADD CONSTRAINT R_7  FOREIGN KEY (id_ticket) REFERENCES Ticket (id_ticket)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;
