package homework03_2;

import java.util.Arrays;

public class Median {

    public static float median(int[] digits) {
        float result;
        Arrays.sort(digits);
        int med_index;
        if (digits.length % 2 == 1) {
            med_index = digits.length / 2;
            result = digits[med_index];
        } else {
            med_index = digits.length / 2;
            result = (digits[med_index] + digits[med_index - 1]) / 2.0f;
        }
        return result;
    }

    public static double median(double[] digits) {
        double result;
        Arrays.sort(digits);
        int med_index;
        if (digits.length % 2 == 1) {
            med_index = digits.length / 2;
            result = digits[med_index];
        } else {
            med_index = digits.length / 2;
            result = (digits[med_index] + digits[med_index - 1]) / 2.0;
        }
        return result;
    }
}
