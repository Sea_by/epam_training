package com.epam.sergeev.homework09_2;

import java.util.HashSet;

public class HashSetCalculator {

    public HashSet associathion(HashSet first, HashSet second) {
        HashSet result = (HashSet) first.clone();
        result.addAll(second);
        return result;
    }

    public HashSet difference(HashSet first, HashSet second) {
        HashSet result = (HashSet) first.clone();
        result.removeAll(second);
        return result;
    }

    public HashSet crossing(HashSet first, HashSet second) {
        HashSet result = new HashSet();
        first.stream().filter((item)
                -> (second.contains(item))).forEachOrdered((item) -> {
            result.add(item);
        });
        return result;
    }

    public HashSet symmetricDifference(HashSet first, HashSet second) {
        HashSet result = associathion(first, second);
        HashSet result1 = crossing(first, second);
        result.removeAll(result1);
        return result;
    }

}
