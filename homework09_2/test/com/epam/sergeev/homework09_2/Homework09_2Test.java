package com.epam.sergeev.homework09_2;

import java.util.HashSet;
import org.junit.Assert;
import org.junit.Test;

public class Homework09_2Test {

    private HashSet first;
    private HashSet second;

    private void init() {
        this.first = new HashSet();
        this.first.add(1);
        this.first.add(2);
        this.first.add(3);
        this.first.add(4);

        this.second = new HashSet();
        this.second.add(3);
        this.second.add(4);
        this.second.add(5);
        this.second.add(6);
    }

    @Test
    public void testAssociathion() {
        HashSet result = new HashSet();
        result.add(1);
        result.add(2);
        result.add(3);
        result.add(4);
        result.add(5);
        result.add(6);

        init();
        HashSetCalculator hsc = new HashSetCalculator();

        Assert.assertEquals(result, hsc.associathion(first, second));
    }

    @Test
    public void testDifference() {
        HashSet result = new HashSet();
        result.add(1);
        result.add(2);

        init();
        HashSetCalculator hsc = new HashSetCalculator();

        Assert.assertEquals(result, hsc.difference(first, second));
    }

    @Test
    public void testCrossing() {
        HashSet result = new HashSet();
        result.add(3);
        result.add(4);

        init();
        HashSetCalculator hsc = new HashSetCalculator();

        Assert.assertEquals(result, hsc.crossing(first, second));
    }

    @Test
    public void testSymmetricDifference() {
        HashSet result = new HashSet();
        result.add(1);
        result.add(2);
        result.add(5);
        result.add(6);

        init();
        HashSetCalculator hsc = new HashSetCalculator();

        Assert.assertEquals(result, hsc.symmetricDifference(first, second));
    }

}
