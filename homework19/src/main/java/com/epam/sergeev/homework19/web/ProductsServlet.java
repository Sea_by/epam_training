package com.epam.sergeev.homework19.web;

import com.epam.sergeev.homework19.model.Product;
import com.epam.sergeev.homework19.model.Ticket;
import com.epam.sergeev.homework19.service.Cashbox;
import com.epam.sergeev.homework19.service.ProductService;
import com.epam.sergeev.homework19.service.ProductServiceImpl;
import com.epam.sergeev.homework19.service.ProductTicketService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/products")
public class ProductsServlet extends HttpServlet {

    private final List<Product> order = new ArrayList<>();
    private ProductService productService;
    private ProductTicketService productTicketService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        productService = new ProductServiceImpl();
        req.setAttribute("products", productService.getAll());
        req.setAttribute("order", order);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/products.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        productService = new ProductServiceImpl();
        productTicketService = new ProductTicketService();
        Cashbox cashbox = new Cashbox();

        String[] selected;

        if (req.getParameter("Submit") == null) {
            selected = req.getParameterValues("products");
            if (selected != null) {
                for (String item : selected) {
                    order.add(productService.getProduct(item));
                }
            }
            req.setAttribute("order", order);
            resp.sendRedirect("products");
        } else {
            Cookie userName = (Cookie) req.getSession().getAttribute("user");
            Ticket ticket = cashbox.getTicket(order, userName.getValue());

            req.setAttribute("summary", productTicketService.getByTicket(ticket));
            req.setAttribute("sum", ticket.getTotal());

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ticket.jsp");
            rd.forward(req, resp);
        }
    }

}
