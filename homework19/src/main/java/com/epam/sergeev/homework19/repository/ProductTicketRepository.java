package com.epam.sergeev.homework19.repository;

import com.epam.sergeev.homework19.model.ProductTicket;
import com.epam.sergeev.homework19.model.Ticket;
import java.util.List;
import javafx.util.Pair;

public interface ProductTicketRepository {

    public void saveProductTicket(ProductTicket productTicket);

    public List<Pair> getByTicket(Ticket ticket);

}
