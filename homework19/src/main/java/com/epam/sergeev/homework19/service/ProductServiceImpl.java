package com.epam.sergeev.homework19.service;

import com.epam.sergeev.homework19.repository.h2.H2ProductRepositoryImpl;
import com.epam.sergeev.homework19.model.Product;
import com.epam.sergeev.homework19.repository.ProductRepository;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository = H2ProductRepositoryImpl.getInstance();

    @Override
    public void saveProduct(Product product) {
        productRepository.addProduct(product);
    }

    @Override
    public Product getProduct(String name) {
        return productRepository.getProductByName(name);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @Override
    public void saveOrder(List<Product> products) {

    }

}
