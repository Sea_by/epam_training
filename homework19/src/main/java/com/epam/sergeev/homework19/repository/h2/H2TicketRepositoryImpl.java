package com.epam.sergeev.homework19.repository.h2;

import com.epam.sergeev.homework19.CloseableSession;
import com.epam.sergeev.homework19.config.HibernateUtil;
import com.epam.sergeev.homework19.filters.RequestLoggingFilter;
import com.epam.sergeev.homework19.model.Ticket;
import com.epam.sergeev.homework19.repository.TicketRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;

public class H2TicketRepositoryImpl implements TicketRepository {

    private static H2TicketRepositoryImpl instance;

    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2TicketRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new H2TicketRepositoryImpl();
        }
        return instance;
    }

    private H2TicketRepositoryImpl() {
    }

    @Override
    public void addTicket(Ticket ticket) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            session.delegate().save(ticket);
            session.delegate().getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error query addTicket", e);
        }
    }

    @Override
    public int getTicketId(Ticket ticket) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            ticket = (Ticket) session.delegate().load(Ticket.class, ticket.getUser().getId());
            session.delegate().getTransaction().commit();
            return ticket.getId();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error query getUserByName ", e);
        }
        return 0;
    }
}
