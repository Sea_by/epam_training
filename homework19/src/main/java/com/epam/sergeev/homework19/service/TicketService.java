package com.epam.sergeev.homework19.service;

import com.epam.sergeev.homework19.repository.h2.H2TicketRepositoryImpl;
import com.epam.sergeev.homework19.model.Ticket;
import com.epam.sergeev.homework19.repository.TicketRepository;

public class TicketService {

    private final TicketRepository ticketRepository = H2TicketRepositoryImpl.getInstance();

    public void addTicket(Ticket ticket) {
        ticketRepository.addTicket(ticket);
    }

    public int getTicketId(Ticket ticket) {
        return ticketRepository.getTicketId(ticket);
    }
}
