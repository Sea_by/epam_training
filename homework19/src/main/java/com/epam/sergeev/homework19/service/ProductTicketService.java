package com.epam.sergeev.homework19.service;

import com.epam.sergeev.homework19.repository.h2.H2ProductTicketRepositoryImpl;
import com.epam.sergeev.homework19.model.ProductTicket;
import com.epam.sergeev.homework19.model.Ticket;
import com.epam.sergeev.homework19.repository.ProductTicketRepository;
import java.util.List;
import javafx.util.Pair;

public class ProductTicketService {

    ProductTicketRepository productTicketRepository = H2ProductTicketRepositoryImpl.getInstance();

    public void save(ProductTicket productTicket) {
        productTicketRepository.saveProductTicket(productTicket);
    }

    public List<Pair> getByTicket(Ticket ticket) {
        return productTicketRepository.getByTicket(ticket);
    }
}
