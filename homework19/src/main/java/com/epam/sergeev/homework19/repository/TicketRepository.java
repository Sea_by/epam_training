package com.epam.sergeev.homework19.repository;

import com.epam.sergeev.homework19.model.Ticket;

public interface TicketRepository {

    public void addTicket(Ticket ticket);

    public int getTicketId(Ticket ticket);
}
