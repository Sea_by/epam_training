package com.epam.sergeev.homework19.repository.h2;

import com.epam.sergeev.homework19.CloseableSession;
import com.epam.sergeev.homework19.config.HibernateUtil;
import com.epam.sergeev.homework19.filters.RequestLoggingFilter;
import com.epam.sergeev.homework19.model.User;
import com.epam.sergeev.homework19.repository.UserRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

public class H2UserRepositoryImpl implements UserRepository {

    public static H2UserRepositoryImpl instance;
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2UserRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new H2UserRepositoryImpl();
        }
        return instance;
    }

    private H2UserRepositoryImpl() {
    }

    @Override
    public void saveUser(String name) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            session.delegate().save(new User(name));
            session.delegate().getTransaction().commit();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "error saveUser query ", ex);
        }
    }

    @Override
    public User getUserByName(String name) {
        User defUser = new User("defUser");
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            Query query = session.delegate().createQuery("from User where name = :name");
            query.setParameter("name", name);
            return (User) query.uniqueResult();
        } catch (Exception sqlEx) {
            LOG.log(Level.SEVERE, "error query getUser ", sqlEx);
        }
        return defUser;
    }
}
