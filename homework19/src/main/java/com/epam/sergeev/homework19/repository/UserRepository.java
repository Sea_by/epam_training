package com.epam.sergeev.homework19.repository;

import com.epam.sergeev.homework19.model.User;

public interface UserRepository {

    public void saveUser(String name);

    public User getUserByName(String name);
}
