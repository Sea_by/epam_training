package com.epam.sergeev.homework19.service;

import com.epam.sergeev.homework19.repository.h2.H2UserRepositoryImpl;
import com.epam.sergeev.homework19.model.User;
import com.epam.sergeev.homework19.repository.UserRepository;

public class UserService {

    private final UserRepository userRepository = H2UserRepositoryImpl.getInstance();

    public void saveUser(String name) {
        userRepository.saveUser(name);
    }

    public User getUserByName(String userName) {
        return userRepository.getUserByName(userName);
    }
}
