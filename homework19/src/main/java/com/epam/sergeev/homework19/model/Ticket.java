package com.epam.sergeev.homework19.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ticket")
    private int id_ticket;

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;
    private double total;

    public Ticket() {
    }

    public Ticket(User user, double total) {
        this.user = user;
        this.total = total;
    }

    public int getId() {
        return id_ticket;
    }

    public void setId(int id) {
        this.id_ticket = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public User getUser() {
        return user;
    }

}
