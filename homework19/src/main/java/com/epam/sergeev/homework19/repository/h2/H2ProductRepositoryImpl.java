package com.epam.sergeev.homework19.repository.h2;

import com.epam.sergeev.homework19.CloseableSession;
import com.epam.sergeev.homework19.config.HibernateUtil;
import com.epam.sergeev.homework19.filters.RequestLoggingFilter;
import com.epam.sergeev.homework19.model.Product;
import com.epam.sergeev.homework19.repository.ProductRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;

public final class H2ProductRepositoryImpl implements ProductRepository {

    private final Product defProduct = new Product("defProduct", 0.0);

    private static H2ProductRepositoryImpl instance;
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    public static synchronized H2ProductRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new H2ProductRepositoryImpl();
        }
        return instance;
    }

    private H2ProductRepositoryImpl() {
    }

    @Override
    public void addProduct(Product product) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            session.delegate().save(product);
            session.delegate().getTransaction().commit();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "error query addProduct ", ex);
        }
    }

    @Override
    public Product getProductByName(String name) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            Query query = session.delegate().createQuery("from Product where name= :name");
            query.setParameter("name", name);
            return (Product) query.uniqueResult();

        } catch (Exception sqlEx) {
            LOG.log(Level.SEVERE, "error query getProduct ", sqlEx);
        }
        return defProduct;
    }

    @Override
    public List<Product> getAll() {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            return session.delegate().createCriteria(Product.class).list();
        } catch (Exception sqlEx) {
            LOG.log(Level.SEVERE, "error query getProducts ", sqlEx);
        }
        return new ArrayList<>();
    }

    @Override
    public Product getProductById(int id_product) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            return (Product) session.delegate().load(Product.class, id_product);
        } catch (Exception sqlEx) {
            LOG.log(Level.SEVERE, "error query getProductById ", sqlEx);
        }
        return defProduct;
    }

}
