package com.epam.sergeev.homework16.service;

import com.epam.sergeev.homework16.model.Product;
import java.util.List;

public interface ProductService {

    public void saveProduct(Product product);

    public Product getProduct(String name);

    public List<Product> getAll();
}
