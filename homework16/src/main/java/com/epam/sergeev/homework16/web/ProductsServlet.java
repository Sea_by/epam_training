package com.epam.sergeev.homework16.web;

import com.epam.sergeev.homework16.model.Product;
import com.epam.sergeev.homework16.service.Calculator;
import com.epam.sergeev.homework16.service.ProductService;
import com.epam.sergeev.homework16.service.ProductServiceImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/products")
public class ProductsServlet extends HttpServlet {

    private final List<Product> order = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService productService = new ProductServiceImpl();
        req.setAttribute("products", productService.getAll());
        req.setAttribute("order", order);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/products.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("Submit") == null) {
            ProductService productService = new ProductServiceImpl();
            String[] selected = req.getParameterValues("products");

            if (selected != null) {
                for (String item : selected) {
                    order.add(productService.getProduct(item));
                }
            }

            req.setAttribute("order", order);
            resp.sendRedirect("products");
        } else {
            req.setAttribute("order", order);

            Calculator calculator = new Calculator();
            req.setAttribute("sum", calculator.calculate(order));

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ticket.jsp");
            rd.forward(req, resp);
        }
    }

}
