package com.epam.sergeev.homework16.service;

import com.epam.sergeev.homework16.model.Product;
import java.util.List;

public class Calculator {

    public double calculate(List<Product> products) {
        double sum = 0;

        for (Product item : products) {
            sum += item.getPrice();
        }
        return sum;
    }
}
