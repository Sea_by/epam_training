package com.epam.sergeev.homework16.repo;

import com.epam.sergeev.homework16.model.Product;
import java.util.ArrayList;
import java.util.List;

public class ProductRepo {

    private final List<Product> products;
    private final Product defProduct = new Product("defProduct", 0);

    private static ProductRepo instance;

    public static synchronized ProductRepo getInstance() {
        if (instance == null) {
            instance = new ProductRepo();
        }
        return instance;
    }

    public ProductRepo() {
        products = new ArrayList<>();
        products.add(new Product("apple", 5));
        products.add(new Product("banana", 3));
        products.add(new Product("glass", 9));
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public Product getProduct(String name) {
        for (Product item : products) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return defProduct;
    }

    public List<Product> getAll() {
        return products;
    }

}
