package com.epam.sergeev.homework16.service;

import com.epam.sergeev.homework16.repo.ProductRepo;
import com.epam.sergeev.homework16.model.Product;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo = ProductRepo.getInstance();

    @Override
    public void saveProduct(Product product) {
        productRepo.addProduct(product);
    }

    @Override
    public Product getProduct(String name) {
        return productRepo.getProduct(name);
    }

    @Override
    public List<Product> getAll() {
        return productRepo.getAll();
    }

}
