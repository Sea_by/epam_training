package com.epam.sergeev.homework09_1;

import java.util.Comparator;

public class SumComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        int firstSum = 0, secondSum = 0;
        final String first = o1.toString();
        final String second = o2.toString();

        for (int i = 0; i < first.length(); i++) {
            firstSum += (first.charAt(i) - 48);
        }

        for (int i = 0; i < second.length(); i++) {
            secondSum += (second.charAt(i) - 48);
        }

        return Integer.valueOf(firstSum).compareTo(secondSum);
    }

}
