package com.epam.sergeev.homework09_1;

import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

public class Homework09_1Test {

    @Test
    public void sortTest() {
        Integer[] inputArray = new Integer[]{121, 4325, 1234, 132, 765, 34, 21, 9};
        Integer[] resultArray = new Integer[]{21, 121, 132, 34, 9, 1234, 4325, 765};

        Arrays.sort(inputArray, new SumComparator());

        Assert.assertArrayEquals(resultArray, inputArray);
    }

}
