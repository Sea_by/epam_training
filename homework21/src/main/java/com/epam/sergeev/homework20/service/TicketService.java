package com.epam.sergeev.homework20.service;

import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    public void addTicket(Ticket ticket) {
        ticketRepository.addTicket(ticket);
    }

    public int getTicketId(Ticket ticket) {
        return ticketRepository.getTicketId(ticket);
    }
}
