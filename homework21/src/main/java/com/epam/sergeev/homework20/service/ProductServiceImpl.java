package com.epam.sergeev.homework20.service;

import com.epam.sergeev.homework20.model.Product;
import com.epam.sergeev.homework20.repository.ProductRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void saveProduct(Product product) {
        productRepository.addProduct(product);
    }

    @Override
    public Product getProduct(String name) {
        return productRepository.getProductByName(name);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @Override
    public void saveOrder(List<Product> products) {

    }

}
