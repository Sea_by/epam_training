package com.epam.sergeev.homework20.web;

import com.epam.sergeev.homework20.service.UserService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginServlet {

    private final String ON = "on";
    private final String USER = "user";

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String doGet() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ON.equals(req.getParameter("checkbox"))) {
            HttpSession session = req.getSession();
            session.setMaxInactiveInterval(3 * 60);
            Cookie userName = new Cookie(USER, req.getParameter(USER));
            userName.setMaxAge(60);
            session.setAttribute(USER, userName);
            userService.saveUser(req.getParameter(USER));
            resp.addCookie(userName);

            resp.sendRedirect("/products");
        } else {
            resp.sendRedirect("/403.html");
        }
    }
}
