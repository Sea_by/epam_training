package com.epam.sergeev.homework20.web;

import com.epam.sergeev.homework20.mail.Mailler;
import com.epam.sergeev.homework20.model.Product;
import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.service.Cashbox;
import com.epam.sergeev.homework20.service.ProductService;
import com.epam.sergeev.homework20.service.ProductTicketService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/products")
public class ProductsServlet {

    private final List<Product> order = new ArrayList<>();

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductTicketService productTicketService;
    @Autowired
    private Cashbox cashbox;
    @Autowired
    private Mailler mailler;

    @RequestMapping(method = RequestMethod.GET)
    protected Model doGet(HttpServletRequest req, HttpServletResponse resp, Model model)
            throws ServletException, IOException {

        model.addAttribute("products", productService.getAll());
        model.addAttribute("order", order);

        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    protected String doPost(HttpServletRequest req, HttpServletResponse resp, Model model)
            throws ServletException, IOException {

        String[] selected;

        if (req.getParameter("Submit") == null) {
            selected = req.getParameterValues("products");
            if (selected != null) {
                for (String item : selected) {
                    order.add(productService.getProduct(item));
                }
            }
            req.setAttribute("order", order);
            resp.sendRedirect("/products");
        } else {
            Cookie userName = (Cookie) req.getSession().getAttribute("user");
            Ticket ticket = cashbox.getTicket(order, userName.getValue());
            double total = ticket.getTotal();

            List<Pair> byTicket = productTicketService.getByTicket(ticket);
            mailler.sendMail(userName.getValue(), byTicket, total);

            req.setAttribute("summary", byTicket);
            req.setAttribute("sum", total);
        }
        return "forward:/ticket.jsp";
    }
}
