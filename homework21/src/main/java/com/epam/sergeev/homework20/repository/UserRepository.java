package com.epam.sergeev.homework20.repository;

import com.epam.sergeev.homework20.model.User;

public interface UserRepository {

    public void saveUser(String name);

    public User getUserByName(String name);
}
