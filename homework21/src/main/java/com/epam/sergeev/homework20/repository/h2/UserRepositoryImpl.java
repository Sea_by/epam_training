package com.epam.sergeev.homework20.repository.h2;

import com.epam.sergeev.homework20.CloseableSession;
import com.epam.sergeev.homework20.filters.RequestLoggingFilter;
import com.epam.sergeev.homework20.model.User;
import com.epam.sergeev.homework20.repository.UserRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    @Override
    public void saveUser(String name) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            session.delegate().save(new User(name));
            session.delegate().getTransaction().commit();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "error saveUser query ", ex);
        }
    }

    @Override
    public User getUserByName(String name) {
        User defUser = new User("defUser");
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            Query query = session.delegate().createQuery("from User where name = :name");
            query.setParameter("name", name);
            return (User) query.uniqueResult();
        } catch (Exception sqlEx) {
            LOG.log(Level.SEVERE, "error query getUser ", sqlEx);
        }
        return defUser;
    }
}
