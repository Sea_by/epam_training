package com.epam.sergeev.homework20.repository;

import com.epam.sergeev.homework20.model.Product;
import java.util.List;

public interface ProductRepository {

    public void addProduct(Product product);

    public Product getProductByName(String name);

    public List<Product> getAll();

    public Product getProductById(int id);
}
