package com.epam.sergeev.homework20.mail;

import java.util.List;
import javafx.util.Pair;

public interface Mailler {

    public void sendMail(String name, List<Pair> byTicket, double total);
}
