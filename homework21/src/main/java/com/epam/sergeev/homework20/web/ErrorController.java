package com.epam.sergeev.homework20.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ErrorController {

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public Model accessDenied(Model model) {
        return model;
    }

    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public Model noMapping(Model model) {
        return model;
    }
}
