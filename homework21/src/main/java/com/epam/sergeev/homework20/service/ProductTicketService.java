package com.epam.sergeev.homework20.service;

import com.epam.sergeev.homework20.model.ProductTicket;
import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.repository.ProductTicketRepository;
import java.util.List;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductTicketService {

    @Autowired
    private ProductTicketRepository productTicketRepository;

    public void save(ProductTicket productTicket) {
        productTicketRepository.saveProductTicket(productTicket);
    }

    public List<Pair> getByTicket(Ticket ticket) {
        return productTicketRepository.getByTicket(ticket);
    }
}
