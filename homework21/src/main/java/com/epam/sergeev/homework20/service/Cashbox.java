package com.epam.sergeev.homework20.service;

import com.epam.sergeev.homework20.model.Product;
import com.epam.sergeev.homework20.model.ProductTicket;
import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.model.User;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;

public class Cashbox {

    @Autowired
    private TicketService ticketService;
    @Autowired
    private ProductTicketService productTicketService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;

    public Ticket getTicket(List<Product> order, String userName) {
        Map<String, Integer> compressed = new TreeMap<>();
        for (Product item : order) {
            String name = item.getName();
            if (compressed.containsKey(name)) {
                compressed.put(name, compressed.get(name) + 1);
            } else {
                compressed.put(name, 1);
            }
        }

        User user = userService.getUserByName(userName);
        Ticket ticket = new Ticket(user, 0);
        ticketService.addTicket(ticket);
        ticket.setId(ticketService.getTicketId(ticket));
        Product product;
        double sum = 0.0;

        for (Map.Entry<String, Integer> item : compressed.entrySet()) {
            product = productService.getProduct(item.getKey());
            productTicketService.save(new ProductTicket(product, ticket, item.getValue()));
            sum += item.getValue() * product.getPrice();
        }
        ticket.setTotal(sum);

        return ticket;
    }
}
