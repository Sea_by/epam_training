package epam.sergeev.homework07;

public class IntegerValidator extends Validator<Integer> {

    @Override
    public void validate(Integer value) {
        if ((value < 1) || (value > 10)) {
            throw new ValidationFailedException("Not in range [1,10]");
        }
    }

}
