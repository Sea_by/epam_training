package epam.sergeev.homework07;

public class StringValidator extends Validator<String> {

    @Override
    public void validate(String value) {
        if ((value.equals("")) || (!Character.isUpperCase(value.charAt(0)))) {
            throw new ValidationFailedException("First letter not in Upper case");
        }
    }
}
