package epam.sergeev.homework07;

public class ValidationSystem {

    static void validate(Object obj) {
        Class objectClass;

        try {
            objectClass = obj.getClass();
        } catch (NullPointerException ex) {
            throw new ValidationFailedException("null value expected");
        }

        if (objectClass == Integer.class) {
            Validator vl = new IntegerValidator();
            vl.validate(obj);
        }

        if (objectClass == String.class) {
            Validator vl = new StringValidator();
            vl.validate(obj);
        }
    }
}
