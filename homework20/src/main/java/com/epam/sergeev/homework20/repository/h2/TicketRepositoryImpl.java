package com.epam.sergeev.homework20.repository.h2;

import com.epam.sergeev.homework20.CloseableSession;
import com.epam.sergeev.homework20.filters.RequestLoggingFilter;
import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.repository.TicketRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class TicketRepositoryImpl implements TicketRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    @Override
    public void addTicket(Ticket ticket) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            session.delegate().save(ticket);
            session.delegate().getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error query addTicket", e);
        }
    }

    @Override
    public int getTicketId(Ticket ticket) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            ticket = (Ticket) session.delegate().load(Ticket.class, ticket.getUser().getId());
            session.delegate().getTransaction().commit();
            return ticket.getId();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error query getUserByName ", e);
        }
        return 0;
    }
}
