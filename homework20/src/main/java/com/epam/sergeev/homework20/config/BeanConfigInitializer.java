package com.epam.sergeev.homework20.config;

import org.springframework.context.support.GenericApplicationContext;

public class BeanConfigInitializer {

    private static BeanConfigInitializer instance;
    private GenericApplicationContext genericApplicationContext;

    public static synchronized BeanConfigInitializer getInstance() {
        if (instance == null) {
            instance = new BeanConfigInitializer();
        }
        return instance;
    }

    private BeanConfigInitializer() {
    }

    public Object getBean(String name) {
        return genericApplicationContext.getBean(name);
    }

    public void setGenericApplicationContext(GenericApplicationContext genericApplicationContext) {
        this.genericApplicationContext = genericApplicationContext;
    }

}
