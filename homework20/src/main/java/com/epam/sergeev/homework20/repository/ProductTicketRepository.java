package com.epam.sergeev.homework20.repository;

import com.epam.sergeev.homework20.model.ProductTicket;
import com.epam.sergeev.homework20.model.Ticket;
import java.util.List;
import javafx.util.Pair;

public interface ProductTicketRepository {

    public void saveProductTicket(ProductTicket productTicket);

    public List<Pair> getByTicket(Ticket ticket);

}
