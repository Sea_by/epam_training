package com.epam.sergeev.homework20.service;

import com.epam.sergeev.homework20.config.BeanConfigInitializer;
import com.epam.sergeev.homework20.model.Product;
import com.epam.sergeev.homework20.model.ProductTicket;
import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.model.User;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Cashbox {

    private final BeanConfigInitializer beanConfigInitializer = BeanConfigInitializer.getInstance();

    private final TicketService ticketService;
    private final ProductTicketService productTicketService;
    private final UserService userService;
    private final ProductService productService;

    public Cashbox() {
        this.productService = (ProductService) beanConfigInitializer.getBean("productService");
        this.userService = (UserService) beanConfigInitializer.getBean("userService");
        this.productTicketService = (ProductTicketService) beanConfigInitializer.getBean("productTicketService");
        this.ticketService = (TicketService) beanConfigInitializer.getBean("ticketService");
    }

    public Ticket getTicket(List<Product> order, String userName) {
        Map<String, Integer> compressed = new TreeMap<>();
        for (Product item : order) {
            String name = item.getName();
            if (compressed.containsKey(name)) {
                compressed.put(name, compressed.get(name) + 1);
            } else {
                compressed.put(name, 1);
            }
        }

        User user = userService.getUserByName(userName);
        Ticket ticket = new Ticket(user, 0);
        ticketService.addTicket(ticket);
        ticket.setId(ticketService.getTicketId(ticket));
        Product product;
        double sum = 0.0;

        for (Map.Entry<String, Integer> item : compressed.entrySet()) {
            product = productService.getProduct(item.getKey());
            productTicketService.save(new ProductTicket(product, ticket, item.getValue()));
            sum += item.getValue() * product.getPrice();
        }
        ticket.setTotal(sum);

        return ticket;
    }
}
