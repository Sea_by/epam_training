package com.epam.sergeev.homework20.service;

import com.epam.sergeev.homework20.model.User;
import com.epam.sergeev.homework20.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void saveUser(String name) {
        userRepository.saveUser(name);
    }

    public User getUserByName(String userName) {
        return userRepository.getUserByName(userName);
    }
}
