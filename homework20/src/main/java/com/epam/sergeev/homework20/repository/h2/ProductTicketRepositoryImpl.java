package com.epam.sergeev.homework20.repository.h2;

import com.epam.sergeev.homework20.CloseableSession;
import com.epam.sergeev.homework20.filters.RequestLoggingFilter;
import com.epam.sergeev.homework20.model.ProductTicket;
import com.epam.sergeev.homework20.model.Ticket;
import com.epam.sergeev.homework20.repository.ProductTicketRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductTicketRepositoryImpl implements ProductTicketRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger LOG = Logger.getLogger(RequestLoggingFilter.class.getName());

    @Override
    public void saveProductTicket(ProductTicket productTicket) {
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            session.delegate().save(productTicket);
            session.delegate().getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error in saveProductTicket", e);
        }
    }

    @Override
    public List<Pair> getByTicket(Ticket ticket) {
        List<Pair> order = new ArrayList<>();
        try (CloseableSession session = new CloseableSession(sessionFactory.openSession())) {
            session.delegate().beginTransaction();
            Query query = session.delegate().createQuery("from ProductTicket where id_ticket= :id");
            query.setParameter("id", ticket.getId());

            List<ProductTicket> list = query.list();

            for (ProductTicket item : list) {
                order.add(new Pair(item.getProduct().getName(), item.getCount()));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "error query getByTicket ", ex);
        }
        return order;
    }
}
