package com.epam.sergeev.homework20.repository;

import com.epam.sergeev.homework20.model.Ticket;

public interface TicketRepository {

    public void addTicket(Ticket ticket);

    public int getTicketId(Ticket ticket);
}
