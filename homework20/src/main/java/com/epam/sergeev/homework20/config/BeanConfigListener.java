package com.epam.sergeev.homework20.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.springframework.context.annotation.AnnotatedBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;

public class BeanConfigListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        GenericApplicationContext genericApplicationContext = new GenericApplicationContext();
        AnnotatedBeanDefinitionReader reader = new AnnotatedBeanDefinitionReader(genericApplicationContext);
        reader.register(BeanConfig.class);

        BeanConfigInitializer beanConfigInitializer = BeanConfigInitializer.getInstance();
        beanConfigInitializer.setGenericApplicationContext(genericApplicationContext);

        genericApplicationContext.refresh();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
