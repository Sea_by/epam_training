package com.epam.sergeev.homework20.web;

import com.epam.sergeev.homework20.config.BeanConfigInitializer;
import com.epam.sergeev.homework20.service.UserService;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginServlet {

    private final String ON = "on";
    private final String USER = "user";
    private final BeanConfigInitializer beanConfigInitializer = BeanConfigInitializer.getInstance();
    private UserService userService;

    @RequestMapping(path = "/homework20/login", method = RequestMethod.GET)
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/login.html");
    }

    @RequestMapping(path = "/homework20/login", method = RequestMethod.POST)
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ON.equals(req.getParameter("checkbox"))) {
            HttpSession session = req.getSession();
            session.setMaxInactiveInterval(3 * 60);
            Cookie userName = new Cookie(USER, req.getParameter(USER));
            userName.setMaxAge(60);

            session.setAttribute(USER, userName);

            userService = (UserService) beanConfigInitializer.getBean("userService");
            userService.saveUser(req.getParameter(USER));

            req.setAttribute("order", new ArrayList<>());

            resp.addCookie(userName);
            resp.sendRedirect("/products");
        } else {
            resp.sendRedirect("/403.html");
        }
    }
}
