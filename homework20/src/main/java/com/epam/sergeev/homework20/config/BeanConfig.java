package com.epam.sergeev.homework20.config;

import com.epam.sergeev.homework20.repository.ProductRepository;
import com.epam.sergeev.homework20.repository.ProductTicketRepository;
import com.epam.sergeev.homework20.repository.TicketRepository;
import com.epam.sergeev.homework20.repository.UserRepository;
import com.epam.sergeev.homework20.repository.h2.ProductRepositoryImpl;
import com.epam.sergeev.homework20.repository.h2.ProductTicketRepositoryImpl;
import com.epam.sergeev.homework20.repository.h2.TicketRepositoryImpl;
import com.epam.sergeev.homework20.repository.h2.UserRepositoryImpl;
import com.epam.sergeev.homework20.service.ProductService;
import com.epam.sergeev.homework20.service.ProductServiceImpl;
import com.epam.sergeev.homework20.service.ProductTicketService;
import com.epam.sergeev.homework20.service.TicketService;
import com.epam.sergeev.homework20.service.UserService;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages = {"com.epam.sergeev.homework20"})
public class BeanConfig {

    @Bean
    public ProductService productService() {
        return new ProductServiceImpl();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public ProductTicketService productTicketService() {
        return new ProductTicketService();
    }

    @Bean
    public TicketService ticketService() {
        return new TicketService();
    }

    @Bean
    public SessionFactory sessionFactory() {
        return HibernateUtil.getSessionFactory();
    }

    @Bean
    public ProductRepository productRepository() {
        return new ProductRepositoryImpl();
    }

    @Bean
    public TicketRepository ticketRepository() {
        return new TicketRepositoryImpl();
    }

    @Bean
    public ProductTicketRepository productTicketRepository() {
        return new ProductTicketRepositoryImpl();
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepositoryImpl();
    }

    @Bean
    public InternalResourceViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/");
        resolver.setSuffix(".html");
        return resolver;
    }
}
