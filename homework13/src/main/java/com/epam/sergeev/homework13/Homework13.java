package com.epam.sergeev.homework13;

public class Homework13 {

    public static void main(String[] args) {
        int a = 4;
        int p = 3;
        double m1 = 1;
        double m2 = 1;

        System.out.println(calculateG(a, p, m1, m2));
    }

    public static double calculateG(int a, int p, double m1, double m2) {
        return 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / (p * p * (m1 + m2));
    }

}
