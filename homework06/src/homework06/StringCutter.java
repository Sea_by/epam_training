package homework06;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringCutter {

    public String[] getTokensSortedArray(String inputString) {
        Set<String> resultSet = new HashSet<>();
        String[] result = inputString.toLowerCase().split("\\W");

        resultSet.addAll(Arrays.asList(result));
        resultSet.remove(""); //if in text was double punctuation

        result = resultSet.toArray(new String[resultSet.size()]);
        Arrays.sort(result);

        return result;
    }

    public void print(String[] strings) {
        char ch = 0;
        for (String item : strings) {
            if (ch != item.charAt(0)) {
                ch = item.charAt(0);
                System.out.println();
                System.out.print(ch + ": ");
            }
            System.out.print(item + " ");
        }
    }
}
