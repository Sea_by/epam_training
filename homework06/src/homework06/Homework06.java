package homework06;

public class Homework06 {

    public static void main(String[] args) {
        String s = "Once upon a time a Wolf was lapping at a spring on a hillside,"
                + " when, looking up, what should he see but a\n"
                + "Lamb just beginning to drink a little lower down.";

        StringCutter sc = new StringCutter();

        sc.print(sc.getTokensSortedArray(s));
    }
}
