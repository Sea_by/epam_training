package homework06;

import org.junit.Test;
import static org.junit.Assert.*;

public class StringCutterTest {

    @Test
    public void testGetTokensSortedArray() {

        String inputString = "Once upon a time a Wolf was lapping at a spring on"
                + " a hillside, when, looking up, what should he see but a\n"
                + "Lamb just beginning to drink a little lower down.";
        StringCutter instance = new StringCutter();
        String[] result = new String[]{"a", "at", "beginning", "but", "down",
            "drink", "he", "hillside", "just", "lamb", "lapping", "little",
            "looking", "lower", "on", "once", "see", "should", "spring", "time",
            "to", "up", "upon", "was", "what", "when", "wolf"};
        assertArrayEquals(result, instance.getTokensSortedArray(inputString));
    }

}
