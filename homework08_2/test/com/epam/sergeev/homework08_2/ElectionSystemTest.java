package com.epam.sergeev.homework08_2;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class ElectionSystemTest {

    @Test
    public void testElect() {
        //first team
        List<Worker> workers = new ArrayList<>();
        List<SkillsEnum> skills = new ArrayList<>();

        skills.add(SkillsEnum.MASON);
        workers.add(new Worker(skills, "Same"));

        skills.add(SkillsEnum.ARCHITECT);
        workers.add(new Worker(skills, "Edd"));

        Team team = new Team(150, workers);

        //second team
        workers = new ArrayList<>();
        skills = new ArrayList<>();

        skills.add(SkillsEnum.CRANE_OPERATOR);
        workers.add(new Worker(skills, "Roland"));

        skills.remove(SkillsEnum.CRANE_OPERATOR);
        skills.add(SkillsEnum.ARCHITECT);
        workers.add(new Worker(skills, "Edd"));

        Team team2 = new Team(130, workers);

        //teams
        List<Team> teams = new ArrayList<>();
        teams.add(team);
        teams.add(team2);

        //requsition
        List<Requisition> reqList = new ArrayList<>();
        reqList.add(new Requisition(SkillsEnum.MASON, 2));

        ElectionSystem es = new ElectionSystem(teams, reqList);
        Assert.assertEquals(1, es.elect());

    }
}
