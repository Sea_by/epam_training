package com.epam.sergeev.homework08_2;

public class Requisition {

    private final SkillsEnum skill;
    private int size;

    public Requisition(SkillsEnum skill, int size) {
        this.skill = skill;
        this.size = size;
    }

    public SkillsEnum getSkill() {
        return skill;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
