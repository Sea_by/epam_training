package com.epam.sergeev.homework08_2;

public enum SkillsEnum {
    PAINTER,
    PLASTERER,
    MASON,
    ARCHITECT,
    TECHNOLOGIST,
    CRANE_OPERATOR
}
