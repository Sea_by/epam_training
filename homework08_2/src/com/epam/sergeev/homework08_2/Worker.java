package com.epam.sergeev.homework08_2;

import java.util.ArrayList;
import java.util.List;

public class Worker {

    private final List<SkillsEnum> skills;
    private final String name;

    public Worker(String name) {
        this.name = name;
        skills = new ArrayList<>();
    }

    public Worker(List<SkillsEnum> skills, String name) {
        this.skills = skills;
        this.name = name;
    }

    public void addSkill(SkillsEnum skill) {
        skills.add(skill);
    }

    public List<SkillsEnum> getSkills() {
        return skills;
    }
}
