package com.epam.sergeev.homework08_2;

import java.util.List;

public class Team {

    private final int price;
    List<Worker> workers;

    public Team(int price, List<Worker> workers) {
        this.price = price;
        this.workers = workers;
    }

    public void addWorker(Worker worker) {
        this.workers.add(worker);
    }

    public int getPrice() {
        return price;
    }

}
