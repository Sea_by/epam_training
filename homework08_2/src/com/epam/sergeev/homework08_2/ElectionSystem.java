package com.epam.sergeev.homework08_2;

import java.util.ArrayList;
import java.util.List;

public class ElectionSystem {

    private final List<Team> teams;
    private final List<Requisition> requisitions;

    public ElectionSystem(List<Team> teams, List<Requisition> requisitions) {
        this.teams = teams;
        this.requisitions = requisitions;
    }

    public int elect() {
        List<Requisition> offer;
        int counter = 0;
        int sum = Integer.MAX_VALUE;
        int answer = 0;
        for (Team team : teams) {
            counter++;
            offer = convertToOffer(team);
            if (!isComply(offer)) {
                if (sum > team.getPrice()) {
                    sum = team.getPrice();
                    answer = counter;
                }
            }
        }
        return answer;
    }

    private List<Requisition> convertToOffer(Team team) {
        List<Requisition> offer = new ArrayList<>();
        team.workers.forEach((man) -> {
            man.getSkills().forEach((skill) -> {
                if (isContains(offer, skill)) {
                    increaseSize(offer, skill);
                } else {
                    offer.add(new Requisition(skill, 0));
                }
            });
        });
        return offer;
    }

    private boolean isContains(List<Requisition> offer, SkillsEnum skill) {
        return offer.stream().anyMatch((item) -> (item.getSkill().equals(skill)));
    }

    private void increaseSize(List<Requisition> offer, SkillsEnum skill) {
        offer.stream().filter((item) -> (item.getSkill().equals(skill))).forEachOrdered((item) -> {
            item.setSize(item.getSize() + 1);
        });
    }

    private boolean isComply(List<Requisition> offer) {
        for (Requisition req : requisitions) {
            for (Requisition item : offer) {
                if ((item.getSkill().equals(req.getSkill()))
                        && (item.getSize() < req.getSize())) {
                    return false;
                }
            }
        }
        return true;
    }

}
