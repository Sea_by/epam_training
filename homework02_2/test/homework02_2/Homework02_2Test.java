package homework02_2;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Homework02_2Test {

    public Homework02_2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(expected = NullPointerException.class)
    public void testMain() {
        String[] args = null;
        Homework02_2.main(args);
    }

    @Test
    public void testGetFactorial() {
        int n = 5;
        long factorialDoWhile = Homework02_2.getFactorialDoWhile(n);
        long factorialFor = Homework02_2.getFactorialFor(n);
        long factorialWhile = Homework02_2.getFactorialWhile(n);
        Assert.assertEquals(factorialDoWhile, factorialFor);
        Assert.assertEquals(factorialWhile, factorialFor);
        Assert.assertEquals(factorialDoWhile, factorialWhile);
    }
}
