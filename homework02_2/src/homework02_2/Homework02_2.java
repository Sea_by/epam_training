package homework02_2;

public class Homework02_2 {

    public static void main(String[] args) {
        int algoritmId = Integer.parseInt(args[0]);
        int loopType = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);

        switch (algoritmId) {
            case 1:
                printFirstTwo(n);
                if (n > 2) {
                    switch (loopType) {
                        case 1:
                            printFibonacciWhile(n);
                            break;
                        case 2:
                            printFibonacciDoWhile(n);
                            break;
                        case 3:
                            printFibonacciFor(n);
                            break;
                    }
                }
                break;
            case 2:
                if (n > 0) {
                    switch (loopType) {
                        case 1:
                            System.out.println(getFactorialWhile(n));
                            break;
                        case 2:
                            System.out.println(getFactorialDoWhile(n));
                            break;
                        case 3:
                            System.out.println(getFactorialFor(n));
                            break;
                    }
                    break;
                }
        }
    }

    private static void printFibonacciWhile(int n) {
        int counter = 3;
        int first = 1;
        int second = 1;
        while (counter <= n) {
            int temp = first;
            first = second;
            second = second + temp;
            System.out.print(" " + second);
            counter++;
        }
    }

    private static void printFibonacciFor(int n) {
        int counter = 3;
        int first = 1;
        int second = 1;
        do {
            int temp = first;
            first = second;
            second = second + temp;
            System.out.print(" " + second);
            counter++;
        } while (counter != n);
    }

    private static void printFibonacciDoWhile(int n) {
        int first = 1;
        int second = 1;
        for (int i = 3; i <= n; i++) {
            int temp = first;
            first = second;
            second = second + temp;
            System.out.print(" " + second);
        }
    }

    public static long getFactorialWhile(int n) {
        long sum = 1;
        int counter = 1;
        while (counter <= n) {
            sum *= counter;
            counter++;
        }
        return sum;
    }

    public static long getFactorialDoWhile(int n) {
        int sum = 1;
        int counter = 1;
        do {
            sum *= counter;
            counter++;
        } while (counter <= n);
        return sum;
    }

    public static long getFactorialFor(int n) {
        long sum = 1;
        for (int i = 1; i <= n; i++) {
            sum *= i;
        }
        return sum;
    }

    private static void printFirstTwo(int n) {
        for (int i = 0; i < n && i < 2; i++) {
            System.out.println(" 1");
        }
    }
}
