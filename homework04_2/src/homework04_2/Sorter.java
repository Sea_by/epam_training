package homework04_2;

interface Sorter {

    int[] execute(int[] array);
}
