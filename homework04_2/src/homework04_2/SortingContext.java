package homework04_2;

public class SortingContext {

    private Sorter sorter;

    public void setSorter(Sorter sorter) {
        this.sorter = sorter;
    }

    public int[] executeSorter(int[] array) {
        return sorter.execute(array);
    }
}
