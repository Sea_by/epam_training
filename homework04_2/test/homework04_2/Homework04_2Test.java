package homework04_2;

import org.junit.Assert;
import org.junit.Test;

public class Homework04_2Test {

    @Test
    public void testBubbleSort() {
        SortingContext context = new SortingContext();
        context.setSorter(new BubbleSort());
        int[] inputArray = new int[]{7, 8, 3, 2, 5, 1, 2, 0, 9};
        int[] resultArray = new int[]{0, 1, 2, 2, 3, 5, 7, 8, 9};
        Assert.assertArrayEquals(resultArray, context.executeSorter(inputArray));
    }

    @Test
    public void testBubbleSelectionSort() {
        SortingContext context = new SortingContext();
        context.setSorter(new SelectionSort());
        int[] inputArray = new int[]{7, 8, 3, 2, 5, 1, 2, 0, 9};
        int[] resultArray = new int[]{0, 1, 2, 2, 3, 5, 7, 8, 9};
        Assert.assertArrayEquals(resultArray, context.executeSorter(inputArray));
    }

}
