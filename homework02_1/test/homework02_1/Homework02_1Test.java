package homework02_1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Homework02_1Test {

    public Homework02_1Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCalculateG() {
        double result = Homework02_1.calculateG(1, 1, 0.3, 0.7);
        Assert.assertEquals(39.478417, result, 0.000001);
    }

    @Test(expected = NullPointerException.class)
    public void testMain_WhenNull() {
        String[] args = null;
        Homework02_1.main(args);
    }

    @Test
    public void testCalculateG_WhenZero() {
        double result = Homework02_1.calculateG(1, 0, 0.3, 0.7);
        Assert.assertEquals(Double.POSITIVE_INFINITY, result, 0.000001);
    }
}
