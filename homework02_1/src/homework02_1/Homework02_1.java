package homework02_1;

public class Homework02_1 {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m1 = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);

        System.out.println(calculateG(a, p, m1, m2));
    }

    public static double calculateG(int a, int p, double m1, double m2) {
        return 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / (p * p * (m1 + m2));
    }

}
